package nl.tno.ids.fl

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import de.fraunhofer.iais.eis.DynamicAttributeTokenBuilder
import de.fraunhofer.iais.eis.InvokeOperationMessageBuilder
import de.fraunhofer.iais.eis.ResultMessageBuilder
import de.fraunhofer.iais.eis.TokenFormat
import de.fraunhofer.iais.eis.util.Util
import nl.tno.ids.base.infomodel.defaults
import nl.tno.ids.base.infomodel.toMultiPartMessage
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.InputStreamEntity
import org.apache.http.impl.client.HttpClients
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.TestPropertySource
import java.io.File
import java.net.URI

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = ["spring.config.location=classpath:application.yaml", "fl.type=WORKER", "fl.backend=http://localhost:48234/cc/api", "fl.trustedComputeNode=urn:ids:TrustedComputeNode"])
@EnableAutoConfiguration
class HealthTest {

    @LocalServerPort
    private lateinit var port: String

    @Test
    fun testHealthCheck() {
        HttpClients.createDefault().execute(HttpGet("http://localhost:$port/health")).use {
            Assertions.assertEquals(200, it.statusLine.statusCode)
        }
    }

    @Test
    fun testInvokeOperationMessage() {
        val invokeOperationMessage = InvokeOperationMessageBuilder()
            ._operationReference_(URI.create("urn:ids:fl:initialize"))
            ._issuerConnector_(URI.create("urn:ids:testReceiver"))
            ._senderAgent_(URI.create("urn:ids:testSender"))
            ._recipientConnector_(URI.create("urn:ids:testReceiver"))
            ._recipientAgent_(URI.create("urn:ids:testReceiver"))
            ._securityToken_(DynamicAttributeTokenBuilder()._tokenFormat_(TokenFormat.SAML_1_1)._tokenValue_("test").build())
            .build()
            .defaults()

        val httpPost = HttpPost("http://localhost:$port/router")
        val file = File.createTempFile("base-data-app-", ".http")
        try {
            val payload = """{
              "workers": ["urn:ids:nlaic:connectors:Erasmus", "urn:ids:nlaic:connectors:Maastro"],
              "rounds": 5,
              "image_shape": [512, 512, 1],
              "patch_shape": [512, 512, 3],
              "log_path": "/home/leroy/app/logs",
              "number_of_augmentations": 2,
            
              "min_bound": -300,
              "max_bound": 200,
              "num_classes": 2,
              "batch_size": 4,
              "num_steps": 100,
              "train_eval_step": 10,
              "val_eval_step": 10,
              "save_model_step": 50,
            
              "learning_rate": 0.0001,
              "decay_steps": 500000,
              "decay_rate": 0.1,
              "opt_momentum": 0.9,
              "dropout_rate": 0.0,
              "l2_loss": 0.0001
            }""".trimIndent()
            val multiPartMessage = invokeOperationMessage.toMultiPartMessage(payload, contentType = "text/plain")
            multiPartMessage.writeToOutputStream(file.outputStream(), true)
            multiPartMessage.httpHeaders.forEach {
                httpPost.addHeader(it.key, it.value)
            }
            httpPost.entity = InputStreamEntity(file.inputStream())

            HttpClients.createDefault().execute(httpPost).use {
                Assertions.assertEquals(200, it.statusLine.statusCode)
                Assertions.assertTrue(it.entity.content.readAllBytes().decodeToString().contains("MessageProcessedNotificationMessage"))
            }
        } finally {
            file.delete()
        }
    }

    companion object {
        private lateinit var wireMock: WireMockServer

        @BeforeAll
        @JvmStatic
        fun setupWiremock() {
            wireMock = WireMockServer(48234).apply { start() }
            val resultMessage = ResultMessageBuilder()
                ._issuerConnector_(URI.create("urn:ids:connector"))
                ._senderAgent_(URI.create("urn:aas:1234"))
                ._recipientConnector_(Util.asList(URI.create("urn:ids:connector")))
                ._recipientAgent_(Util.asList(URI.create("urn:aas:1234")))
                ._correlationMessage_(URI("urn:ids:message:1"))
                .build()
                .defaults()
                .toMultiPartMessage(payload = "[]")

            wireMock.stubFor(
                any(urlMatching("/cc/api.*")).willReturn(ok())
            )
            wireMock.stubFor(
                any(urlMatching("/cc/forward.*"))
                    .willReturn(ok()
                        .withBody(resultMessage.toString())
                        .withHeader("Content-Type", resultMessage.httpHeaders["Content-Type"])
                    )
            )

        }
    }
}