package nl.tno.ids.fl

import de.fraunhofer.iais.eis.ArtifactBuilder
import de.fraunhofer.iais.eis.DataResourceBuilder
import de.fraunhofer.iais.eis.IANAMediaTypeBuilder
import de.fraunhofer.iais.eis.RepresentationBuilder
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.base.ResourcePublisher
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.common.serialization.DateUtil
import org.json.JSONObject
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.util.concurrent.CompletableFuture

@RestController
@RequestMapping("datasets")
class DatasetHandler(
    private val idsConfig: IdsConfig,
    private val resourcePublisher: ResourcePublisher
) {
    private fun createResourceId(key: String): URI {
        return if (idsConfig.connectorId.startsWith("urn:")) {
            URI("${idsConfig.connectorId}:datasets:$key")
        } else {
            URI("${idsConfig.connectorId.dropLastWhile { it == '/' }}/datasets/$key")
        }
    }

    @PostMapping("{key}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun addDataset(@PathVariable key: String, @RequestBody dataset: String): CompletableFuture<ResponseEntity<String>> {
        return CompletableFuture.supplyAsync {
            val datasetJson = JSONObject(dataset)
            val resource = DataResourceBuilder(createResourceId(key))
                ._title_(TypedLiteral(key, "en"))
                ._sovereign_(URI(idsConfig.participantId))
                ._representation_(RepresentationBuilder()
                    ._mediaType_(IANAMediaTypeBuilder()
                        ._filenameExtension_(datasetJson.getString("mediaType"))
                        .build())
                    ._instance_(ArtifactBuilder()
                        ._fileName_(key)
                        ._byteSize_(datasetJson.getBigInteger("byteSize"))
                        ._creationDate_(DateUtil.now())
                        .build())
                    .build())
                ._description_(TypedLiteral(datasetJson.getJSONArray("shape").joinToString(","),"SHAPE"))
                .build()
            resourcePublisher.publishResourceToCoreContainer(resource)
            ResponseEntity.ok("")
        }
    }

    @DeleteMapping("{key}")
    fun deleteDataset(@PathVariable key: String): CompletableFuture<ResponseEntity<String>>  {
        return CompletableFuture.supplyAsync {
            resourcePublisher.unpublishResourceToCoreContainer(createResourceId(key).toString())
            ResponseEntity.ok("")
        }
    }
}