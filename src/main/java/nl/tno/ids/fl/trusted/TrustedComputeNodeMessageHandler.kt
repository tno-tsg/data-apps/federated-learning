package nl.tno.ids.fl.trusted

import de.fraunhofer.iais.eis.InvokeOperationMessage
import de.fraunhofer.iais.eis.RejectionReason
import de.fraunhofer.iais.eis.RequestMessage
import nl.tno.ids.base.MessageHandler
import nl.tno.ids.base.ResponseBuilder
import nl.tno.ids.base.StringMessageHandler
import nl.tno.ids.base.multipart.MultiPartMessage
import nl.tno.ids.fl.FederatedLearningConfig
import org.apache.commons.codec.binary.Base64InputStream
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.InputStreamEntity
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.json.JSONArray
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.core.io.FileSystemResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.util.concurrent.ConcurrentHashMap
import kotlin.io.path.inputStream

@Component
@ConditionalOnProperty("fl.type", havingValue = "TRUSTED_COMPUTE_NODE")
class RequestMessageHandler(
    private val messageOutbox: MessageOutbox,
    private val responseBuilder: ResponseBuilder,
    private val federatedLearningConfig: FederatedLearningConfig
) : StringMessageHandler<RequestMessage> {
    override fun handle(header: RequestMessage, payload: String?, httpHeaders: HttpHeaders): ResponseEntity<*> {
        for (i in 1..(10*federatedLearningConfig.longPollingInterval)) {
            messageOutbox.outbox[header.issuerConnector.toString()]?.let { outboxMessages ->
                if (outboxMessages.isNotEmpty()) {
                    val outboxMessage = outboxMessages.removeFirst()
                    val tmpFile = File.createTempFile("outbox", "http")
                    val outMultiPartMessage =
                        outboxMessage.path?.let { MultiPartMessage(outboxMessage.header, it.inputStream()) }
                            ?: MultiPartMessage(outboxMessage.header, outboxMessage.payload)
                    outMultiPartMessage.writeToOutputStream(tmpFile.outputStream())

                    return ResponseEntity.ok(object : FileSystemResource(tmpFile) {
                        override fun getInputStream(): InputStream {
                            return object : FileInputStream(tmpFile) {
                                override fun close() {
                                    super.close()
                                    file.delete()
                                }
                            }
                        }
                    })
                }
            }
            Thread.sleep(100)
        }
        return responseBuilder.createRejectionResponse(header, RejectionReason.NOT_FOUND, HttpStatus.OK)
    }
}

@Component
@ConditionalOnProperty("fl.type", havingValue = "TRUSTED_COMPUTE_NODE")
class InvokeOperationMessageHandler(
    private val responseBuilder: ResponseBuilder,
    private val federatedLearningConfig: FederatedLearningConfig,
    private val httpClient: CloseableHttpClient
) : MessageHandler<InvokeOperationMessage> {
    var status: MutableMap<String, MutableMap<Long, JSONObject>> = ConcurrentHashMap()

    companion object {
        private val LOG = LoggerFactory.getLogger(InvokeOperationMessageHandler::class.java)
    }

    init {
        println("Test")
    }

    override fun handle(header: InvokeOperationMessage, payload: InputStream?, httpHeaders: HttpHeaders): ResponseEntity<*> {
        LOG.info("Handling InvokeOperationMessage ${header.operationReference} with ${payload?.available()} bytes")
        return when (header.operationReference.toString()) {
            "urn:ids:fl:initialize" -> {
                LOG.info("Received initial train parameters")
                status = ConcurrentHashMap()
                val httpPost = HttpPost("${federatedLearningConfig.backend}/initialize")
                httpPost.entity = InputStreamEntity(payload)
                val responseStatus = httpClient.execute(httpPost).use { response ->
                    response.statusLine.statusCode
                }
                when (responseStatus) {
                    in 200..299 -> responseBuilder.createMessageProcessedResponse(header)
                    in 400..499 -> {
                        LOG.warn("Error in handling in backend: $responseStatus")
                        responseBuilder.createRejectionResponse(header, RejectionReason.BAD_PARAMETERS, HttpStatus.OK)
                    }
                    else -> {
                        LOG.warn("Error in handling in backend: $responseStatus")
                        responseBuilder.createRejectionResponse(
                            header,
                            RejectionReason.INTERNAL_RECIPIENT_ERROR,
                            HttpStatus.OK
                        )
                    }
                }
            }
            "urn:ids:fl:train" -> {
                LOG.info("Received initial train model")
                val httpPost = HttpPost("${federatedLearningConfig.backend}/train")
                httpPost.entity = InputStreamEntity(Base64InputStream(payload))
                val responseStatus = httpClient.execute(httpPost).use { response ->
                    response.statusLine.statusCode
                }
                when (responseStatus) {
                    in 200..299 -> responseBuilder.createMessageProcessedResponse(header)
                    in 400..499 -> {
                        LOG.warn("Error in handling in backend: $responseStatus")
                        responseBuilder.createRejectionResponse(header, RejectionReason.BAD_PARAMETERS, HttpStatus.OK)
                    }
                    else -> {
                        LOG.warn("Error in handling in backend: $responseStatus")
                        responseBuilder.createRejectionResponse(
                            header,
                            RejectionReason.INTERNAL_RECIPIENT_ERROR,
                            HttpStatus.OK
                        )
                    }
                }
            }
            "urn:ids:fl:model" -> {
                LOG.info("Received trained model")
                val httpPost = HttpPost("${federatedLearningConfig.backend}/model")
                httpPost.entity = InputStreamEntity(Base64InputStream(payload))
                httpPost.addHeader("Forward-Sender", header.issuerConnector.toString())
                val responseStatus = httpClient.execute(httpPost).use { response ->
                    response.statusLine.statusCode
                }
                when (responseStatus) {
                    in 200..299 -> responseBuilder.createMessageProcessedResponse(header)
                    in 400..499 -> {
                        LOG.warn("Error in handling in backend: $responseStatus")
                        responseBuilder.createRejectionResponse(header, RejectionReason.BAD_PARAMETERS, HttpStatus.OK)
                    }
                    else -> {
                        LOG.warn("Error in handling in backend: $responseStatus")
                        responseBuilder.createRejectionResponse(
                            header,
                            RejectionReason.INTERNAL_RECIPIENT_ERROR,
                            HttpStatus.OK
                        )
                    }
                }
            }
            "urn:ids:fl:trainstatus" -> {
                LOG.info("Received train status")
                val timestamp = System.currentTimeMillis()
                status[header.issuerConnector.toString()]?.let {
                    it[timestamp] = JSONObject(payload?.readAllBytes()?.decodeToString())
                } ?: run {
                    status[header.issuerConnector.toString()] = ConcurrentHashMap(mapOf(timestamp to JSONObject(payload?.readAllBytes()?.decodeToString())))
                }
                responseBuilder.createMessageProcessedResponse(header)
            }
            "urn:ids:fl:status" -> {
                LOG.info("Received status request")
                ResponseEntity.ok(
                    MultiPartMessage(
                        responseBuilder.createResultMessage(header).build(),
                        JSONArray(status.map { worker ->
                            worker.value.map {
                                it.value.apply { put("worker", worker.key) }
                                it.value.apply { put("timestamp", it.key) }
                            }
                        }).toString()
                    )
                )
            }
            "urn:ids:fl:getModel" -> {
                LOG.info("Received model request")
                val httpGet = HttpGet("${federatedLearningConfig.backend}/model")
                httpClient.execute(httpGet).use { response ->
                    when (response.statusLine.statusCode) {
                        in 200..299 -> {
                            val tmpFile = File.createTempFile("outbox", "http")
                            val multiPartMessage = MultiPartMessage(
                                responseBuilder.createResultMessage(header).build(),
                                response.entity.content
                            )
                            multiPartMessage.writeToOutputStream(tmpFile.outputStream())

                            ResponseEntity.ok(object : FileSystemResource(tmpFile) {
                                override fun getInputStream(): InputStream {
                                    return object : FileInputStream(tmpFile) {
                                        override fun close() {
                                            super.close()
                                            file.delete()
                                        }
                                    }
                                }
                            })
                        }
                        in 400..499 -> {
                            LOG.warn("Error in handling in backend: ${response.statusLine.statusCode}")
                            responseBuilder.createRejectionResponse(
                                header,
                                RejectionReason.BAD_PARAMETERS,
                                HttpStatus.OK
                            )
                        }
                        else -> {
                            LOG.warn("Error in handling in backend: ${response.statusLine.statusCode}")
                            responseBuilder.createRejectionResponse(
                                header,
                                RejectionReason.INTERNAL_RECIPIENT_ERROR,
                                HttpStatus.OK
                            )
                        }
                    }
                }
            }
            else -> {
                LOG.warn("Received unknown operation reference ${header.operationReference}")
                responseBuilder.createRejectionResponse(
                    header,
                    RejectionReason.BAD_PARAMETERS,
                    HttpStatus.BAD_REQUEST
                )
            }
        }
    }
}