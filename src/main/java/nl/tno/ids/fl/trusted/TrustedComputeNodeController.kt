package nl.tno.ids.fl.trusted

import de.fraunhofer.iais.eis.InvokeOperationMessage
import de.fraunhofer.iais.eis.InvokeOperationMessageBuilder
import nl.tno.ids.base.BrokerClient
import nl.tno.ids.base.configuration.IdsConfig
import nl.tno.ids.base.infomodel.defaults
import nl.tno.ids.fl.FederatedLearningConfig
import nl.tno.ids.fl.researcher.DatasetWithConnectors
import org.apache.commons.codec.binary.Base64InputStream
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.CloseableHttpClient
import org.json.JSONArray
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.core.io.FileSystemResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.io.File
import java.io.FileInputStream
import java.io.InputStream
import java.net.URI
import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.CompletableFuture
import kotlin.random.Random

@RestController
@ConditionalOnProperty("fl.type", havingValue = "TRUSTED_COMPUTE_NODE")
class TrustedComputeNodeController(
    private val messageOutbox: MessageOutbox,
    private val idsConfig: IdsConfig,
    private val brokerClient: BrokerClient,
    private val handler: InvokeOperationMessageHandler,
    private val httpClient: CloseableHttpClient,
    private val federatedLearningConfig: FederatedLearningConfig,
) {
    companion object {
        private val LOG = LoggerFactory.getLogger(TrustedComputeNodeController::class.java)
    }

    @GetMapping("researcher/connectors")
    fun listConnectors(): List<String> {
        LOG.info("Received connectors query")
        return brokerClient.updateConnectors().map { it.key }
    }

    @GetMapping("researcher/connectors/datasets")
    fun listConnectorsWithDatasets(): List<DatasetWithConnectors> {
        LOG.info("Received datasets query")
        val connectors = brokerClient.updateConnectors()

        return connectors.flatMap { (connectorId, connector) ->
            connector.resourceCatalog?.flatMap {
                it?.offeredResource?.map { offer ->
                    offer.title.first().value to connectorId
                } ?: emptyList()
            } ?: emptyList()
        }.groupBy { pair -> pair.first }.map { group ->
            DatasetWithConnectors(group.key, group.value.map { it.second })
        }
    }

    @PostMapping("researcher/train")
    fun train(inputStream: InputStream): CompletableFuture<ResponseEntity<String>> {
        return CompletableFuture.supplyAsync {
            val tmpFile = File.createTempFile("model", ".tmp")
            Base64InputStream(inputStream, true).transferTo(tmpFile.outputStream())

            val response = handler.handle(InvokeOperationMessageBuilder()
                ._operationReference_(URI.create("urn:ids:fl:train"))
                ._issuerConnector_(URI.create(idsConfig.connectorId))
                ._senderAgent_(URI.create(idsConfig.participantId))
                ._recipientAgent_(URI.create(idsConfig.participantId))
                ._recipientConnector_(URI.create(idsConfig.connectorId))
                .build()
                .defaults(), tmpFile.inputStream(), HttpHeaders())

            if (response.statusCodeValue != 200) {
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("")
            } else {
                ResponseEntity.ok("")
            }
        }
    }

    @PostMapping("researcher/model")
    fun model(): CompletableFuture<ResponseEntity<*>> {
        return CompletableFuture.supplyAsync {
            val httpGet = HttpGet("${federatedLearningConfig.backend}/model")
            httpClient.execute(httpGet).use { response ->
                when (response.statusLine.statusCode) {
                    in 200..299 -> {
                        val tmpFile = File.createTempFile("outbox", "http")
                        response.entity.writeTo(tmpFile.outputStream())
                        ResponseEntity.ok(object : FileSystemResource(tmpFile) {
                            override fun getInputStream(): InputStream {
                                return object : FileInputStream(tmpFile) {
                                    override fun close() {
                                        super.close()
                                        file.delete()
                                    }
                                }
                            }
                        })
                    }
                    else -> ResponseEntity.status(response.statusLine.statusCode).build()
                }
            }
        }
    }

    @GetMapping("researcher/status", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun status(): ResponseEntity<String> {
        return ResponseEntity.ok(JSONArray(handler.status.map { worker ->
            worker.value.map {
                it.value.apply { put("worker", worker.key) }
                it.value.apply { put("timestamp", it.key) }
            }
        }).toString())
    }


    @PostMapping("researcher/initialize")
    fun initialize(@RequestBody body: String): CompletableFuture<ResponseEntity<String>> {
        return CompletableFuture.supplyAsync {
            val response = handler.handle(InvokeOperationMessageBuilder()
                ._operationReference_(URI.create("urn:ids:fl:initialize"))
                ._issuerConnector_(URI.create(idsConfig.connectorId))
                ._senderAgent_(URI.create(idsConfig.participantId))
                ._recipientAgent_(URI.create(idsConfig.participantId))
                ._recipientConnector_(URI.create(idsConfig.connectorId))
                .build()
                .defaults(), body.byteInputStream(), HttpHeaders())

            if (response.statusCodeValue != 200) {
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("")
            } else {
                ResponseEntity.ok("")
            }
        }
    }

    @PostMapping("init")
    fun initialize(
        @RequestBody body: String,
        @RequestHeader("Forward-To") workerId: String
    ): CompletableFuture<ResponseEntity<String>> {
        LOG.info("Received init forward request from backend")
        return CompletableFuture.supplyAsync {
            messageOutbox.add(
                idsid = workerId,
                header = createInvokeOperationMessage(workerId, "urn:ids:fl:initialize"),
                payload = body
            )
            ResponseEntity.ok("")
        }
    }

    @PostMapping("model")
    fun model(
        inputStream: InputStream,
        @RequestHeader("Forward-To") workerId: String
    ): CompletableFuture<ResponseEntity<String>> {
        LOG.info("Received averaged model request from backend")
        return CompletableFuture.supplyAsync {
            val tmpPath = Paths.get("./${workerId}-${Random.nextInt(10000, 99999)}")
            inputStream.use { input ->
                Files.copy(Base64InputStream(input, true), tmpPath)
            }
            messageOutbox.add(
                idsid = workerId,
                header = createInvokeOperationMessage(workerId, "urn:ids:fl:model"),
                path = tmpPath
            )
            ResponseEntity.ok("")
        }
    }

    @PostMapping("finish")
    fun finish(@RequestHeader("Forward-To") workerId: String): CompletableFuture<ResponseEntity<String>> {
        LOG.info("Received finish request from backend")
        return CompletableFuture.supplyAsync {
            messageOutbox.add(
                idsid = workerId,
                header = createInvokeOperationMessage(workerId, "urn:ids:fl:finish")
            )
            ResponseEntity.ok("")
        }
    }

    private fun createInvokeOperationMessage(workerId: String, operationReference: String): InvokeOperationMessage {
        return InvokeOperationMessageBuilder()
            ._operationReference_(URI.create(operationReference))
            ._issuerConnector_(URI.create(idsConfig.connectorId))
            ._senderAgent_(URI.create(idsConfig.participantId))
            ._recipientAgent_(URI.create(workerId))
            ._recipientConnector_(URI.create(workerId))
            .build()
            .defaults()
    }
}