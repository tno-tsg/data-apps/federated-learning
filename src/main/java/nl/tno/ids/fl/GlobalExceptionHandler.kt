package nl.tno.ids.fl

import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class GlobalExceptionHandler: ResponseEntityExceptionHandler() {
    companion object {
        private val LOG = LoggerFactory.getLogger(Application::class.java)
    }


    @ExceptionHandler(Exception::class)
    fun handleGlobalException(exception: Exception, request: WebRequest): ResponseEntity<Any>? {
        LOG.error("Exception during Spring handling", exception)
        return super.handleException(exception, request)
    }

}