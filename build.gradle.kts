
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.text.SimpleDateFormat
import java.util.*

plugins {
    id("org.springframework.boot") version "2.4.2"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    id("com.google.cloud.tools.jib") version "3.0.0"
    kotlin("jvm") version "1.5.30"
    kotlin("plugin.spring") version "1.5.30"
    kotlin("plugin.serialization") version "1.5.30"
}

group = "nl.tno.ids"
version = "4.1.0-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
    jcenter()
    maven {
        url = uri("https://maven.iais.fraunhofer.de/artifactory/eis-ids-public/")
    }
    maven {
        url = uri("https://nexus.dataspac.es/repository/tsg-maven/")
    }
}

val springVersion = "2.4.+"
val jacksonVersion = "2.12.+"
val tnoIdsVersion = "4.6.1-SNAPSHOT"
val springCloudVersion = "2020.0.3"

dependencies {
    implementation("nl.tno.ids:base-data-app:$tnoIdsVersion")

    implementation("org.springframework.cloud:spring-cloud-dependencies:$springCloudVersion")
    implementation("org.springframework.cloud:spring-cloud-starter-sleuth:3.0.3")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa:$springVersion")

    implementation("org.json:json:20210307")

    testImplementation("org.springframework.boot:spring-boot-starter-test:$springVersion")
    testImplementation("com.github.tomakehurst:wiremock-jre8:2.28.1")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

/**
 * Use Google Container Tools / jib to build and publish a Docker image.
 * This task is ran using Gitlab Runners, defined in .gitlab-ci.yml
 * Publish Docker image to repository
 */
jib {
    to {
        val imageName = System.getenv().getOrDefault("IMAGE_NAME", "docker.nexus.dataspac.es/federated-learning/data-app")
        project.logger.lifecycle("Image tag: ${System.getenv()}")
        val imageTag = System.getenv().getOrDefault("IMAGE_TAG", "develop").replace('/', '-')
        val imageTags = mutableSetOf(imageTag)
        if (System.getenv().containsKey("CI")) {
            imageTags += "${imageTag}-${SimpleDateFormat("yyyyMMddHHmm").format(Date())}"
        }
        image = imageName
        tags = imageTags
    }

    container {
        jvmFlags = listOf("-Xms512m", "-Xmx512m")
        ports = listOf("8080/tcp")
        creationTime = "USE_CURRENT_TIMESTAMP"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
